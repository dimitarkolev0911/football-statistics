package com.football_analytics.statistics.controllers;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.football_analytics.statistics.entities.Fixture;
import com.football_analytics.statistics.models.DetailedStats;
import com.football_analytics.statistics.services.TeamService;

import lombok.AllArgsConstructor;

@RestController
@RequestMapping(value = "fixtures")
@AllArgsConstructor
public class TeamController {

    private final TeamService teamService;

    @GetMapping(path = "/detailed-stats-for-team/{teamName}")
    public DetailedStats getFixtureStatisticsByTeam(@PathVariable(value = "teamName") String teamName) {
        return teamService.getDetailedStatsForTeam(teamName);
    }
}
