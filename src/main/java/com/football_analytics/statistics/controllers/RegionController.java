package com.football_analytics.statistics.controllers;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.football_analytics.statistics.entities.Country;
import com.football_analytics.statistics.entities.Fixture;
import com.football_analytics.statistics.entities.League;
import com.football_analytics.statistics.entities.Region;
import com.football_analytics.statistics.entities.Stadium;
import com.football_analytics.statistics.entities.Team;
import com.football_analytics.statistics.repositories.CountryRepository;
import com.football_analytics.statistics.repositories.FixtureRepository;
import com.football_analytics.statistics.repositories.LeagueRepository;
import com.football_analytics.statistics.repositories.RegionRepository;
import com.football_analytics.statistics.repositories.StadiumRepository;
import com.football_analytics.statistics.repositories.TeamRepository;

import lombok.AllArgsConstructor;

@RestController
@RequestMapping(value = "regions")
@AllArgsConstructor
public class RegionController {

    @Autowired
    RegionRepository regionRepository;

    @Autowired
    CountryRepository countryRepository;

    @Autowired
    LeagueRepository leagueRepository;

    @Autowired
    StadiumRepository stadiumRepository;

    @Autowired
    TeamRepository teamRepository;

    @Autowired
    FixtureRepository fixtureRepository;

    @GetMapping("/regions")
    List<Region> getAllRegions() {
        return regionRepository.findAll();
    }

    @GetMapping("/countries")
    List<Country> getAllCountries() {
        return countryRepository.findAll();
    }

    @GetMapping("/leagues")
    List<League> getAllLeagues() {
        return leagueRepository.findAll();
    }

    @GetMapping("/teams")
    List<Team> getAllTeams() {
        return teamRepository.findAll();
    }

    @GetMapping("/stadiums")
    List<Stadium> getAllStadiums() {
        return stadiumRepository.findAll();
    }

    @GetMapping("/fixtures")
    List<Fixture> getAllFixtures() {
        return fixtureRepository.findAll().stream()
                .filter(fixture -> fixture.getHomeTeamFixtureStatistics() != null)
                .collect(Collectors.toList());
    }
}
