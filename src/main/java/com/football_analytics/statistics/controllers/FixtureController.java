package com.football_analytics.statistics.controllers;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.football_analytics.statistics.entities.Fixture;
import com.football_analytics.statistics.services.FixtureService;

import lombok.AllArgsConstructor;

@RestController
@RequestMapping(value = "fixtures")
@AllArgsConstructor
public class FixtureController {

    private final FixtureService fixtureService;

    @GetMapping(path = "/fixtures-for-team/{teamName}")
    public List<Fixture> getFixturesByTeam(@PathVariable(value = "teamName") String teamName) {
        return fixtureService.getFixturesForTeam(teamName);
    }

}
