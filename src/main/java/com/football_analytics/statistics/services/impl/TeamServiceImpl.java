package com.football_analytics.statistics.services.impl;

import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.hibernate.service.spi.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.football_analytics.statistics.Errors;
import com.football_analytics.statistics.entities.Fixture;
import com.football_analytics.statistics.entities.Team;
import com.football_analytics.statistics.models.BasicStats;
import com.football_analytics.statistics.models.DetailedStats;
import com.football_analytics.statistics.repositories.FixtureRepository;
import com.football_analytics.statistics.repositories.TeamRepository;
import com.football_analytics.statistics.services.FixtureService;
import com.football_analytics.statistics.services.TeamService;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class TeamServiceImpl implements TeamService {

    private final TeamRepository teamRepository;
    private final FixtureRepository fixtureRepository;

    private static final int RECENT_FIXTURES_LIMIT = 5;

    @Override
    public Team getTeamByName(String teamName) {
        if (teamName == null || teamName.trim().length() == 0) {
            throw new ServiceException(Errors.INVALID_TEAM_NAME.getMessage());
        }

        Optional<Team> result = teamRepository.findTeamByName(teamName);
        if (result.isEmpty()) {
            throw new ServiceException(Errors.TEAM_NOT_FOUND.getMessage());
        }

        return result.get();
    }

    @Override
    public DetailedStats getDetailedStatsForTeam(String teamName) {
        Team team = getTeamByName(teamName);
        List<Fixture> fixtures = getFixturesForTeam(team);
        if (CollectionUtils.isEmpty(fixtures)) {
            return DetailedStats.getDefaultDetailedStats();
        }

        List<Fixture> fixturesAsHost = fixtures.stream()
                .filter(fixture -> fixture.getHomeTeam() != null && team.getId().equals(fixture.getHomeTeam().getId()))
                .collect(Collectors.toList());
        List<Fixture> fixturesAsGuest = fixtures.stream()
                .filter(fixture -> fixture.getAwayTeam() != null && team.getId().equals(fixture.getAwayTeam().getId()))
                .collect(Collectors.toList());

        BasicStats basicStats = createBasicStatsFrom(fixturesAsHost, fixturesAsGuest);

        return DetailedStats.builder()
                .basicStats(basicStats)
                .mostRecentFixtures(getMostRecentFixturesFrom(fixtures))
                .mostRecentFixturesAsHost(getMostRecentFixturesFrom(fixturesAsHost))
                .mostRecentFixturesAsAway(getMostRecentFixturesFrom(fixturesAsGuest))
                .build();
    }

    private List<Fixture> getMostRecentFixturesFrom(List<Fixture> fixtures) {
        return fixtures.stream()
                .sorted((fixture1, fixture2) -> fixture2.getDate().compareTo(fixture1.getDate()))
                .limit(RECENT_FIXTURES_LIMIT)
                .collect(Collectors.toList());
    }

    private BasicStats createBasicStatsFrom(List<Fixture> fixturesAsHost, List<Fixture> fixturesAsGuest) {
        double totalCornersFor = 0d;
        double totalCornersAgainst = 0d;
        double totalGoalsFor = 0d;
        double totalGoalsAgainst = 0d;

        for (Fixture fixture: fixturesAsHost) {
            totalCornersFor += fixture.getHomeTeamFixtureStatistics().getCorners();
            totalCornersAgainst += fixture.getAwayTeamFixtureStatistics().getCorners();
            totalGoalsFor += fixture.getHomeTeamFixtureStatistics().getGoals();
            totalGoalsAgainst += fixture.getAwayTeamFixtureStatistics().getGoals();
        }

        for (Fixture fixture: fixturesAsGuest) {
            totalCornersFor += fixture.getAwayTeamFixtureStatistics().getCorners();
            totalCornersAgainst += fixture.getHomeTeamFixtureStatistics().getCorners();
            totalGoalsFor += fixture.getAwayTeamFixtureStatistics().getGoals();
            totalGoalsAgainst += fixture.getHomeTeamFixtureStatistics().getGoals();
        }

        int totalFixtures = fixturesAsHost.size() + fixturesAsGuest.size();
        return BasicStats.builder()
                .avgCorners((totalCornersFor + totalCornersAgainst) / totalFixtures)
                .avgGoals((totalGoalsFor + totalGoalsFor) / totalFixtures)
                .avgCornersFor(totalCornersFor / totalFixtures)
                .avgGoalsFor(totalGoalsFor / totalFixtures)
                .avgCornersAgainst(totalCornersAgainst / totalFixtures)
                .avgGoalsAgainst(totalGoalsAgainst / totalFixtures)
                .build();
    }

    List<Fixture> getFixturesForTeam(Team team) {
        if (team == null || team.getId() == null) {
            throw new ServiceException(Errors.INVALID_TEAM.getMessage());
        }

        return fixtureRepository.findFixturesForTeam(team.getId());
    }
}
