package com.football_analytics.statistics.services.impl;

import java.util.List;

import org.hibernate.service.spi.ServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.football_analytics.statistics.Errors;
import com.football_analytics.statistics.entities.Fixture;
import com.football_analytics.statistics.entities.Team;
import com.football_analytics.statistics.repositories.FixtureRepository;
import com.football_analytics.statistics.services.FixtureService;
import com.football_analytics.statistics.services.TeamService;

import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class FixtureServiceImpl implements FixtureService {

    private final TeamService teamService;
    private final FixtureRepository fixtureRepository;


    @Override
    public List<Fixture> getFixturesForTeam(String teamName) {
        Team team = teamService.getTeamByName(teamName);
        return getFixturesForTeam(team);
    }

    @Override
    public List<Fixture> getFixturesForTeam(Team team) {
        if (team == null || team.getId() == null) {
            throw new ServiceException(Errors.INVALID_TEAM.getMessage());
        }

        return fixtureRepository.findFixturesForTeam(team.getId());
    }
}
