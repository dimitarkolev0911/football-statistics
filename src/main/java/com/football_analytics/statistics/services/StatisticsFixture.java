package com.football_analytics.statistics.services;

import com.football_analytics.statistics.models.BasicStats;

public interface StatisticsFixture {

    BasicStats getBasicStatsForTeam(String teamName);

}
