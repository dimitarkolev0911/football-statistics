package com.football_analytics.statistics.services;

import java.util.List;

import com.football_analytics.statistics.entities.Fixture;
import com.football_analytics.statistics.entities.Team;

public interface FixtureService {

    List<Fixture> getFixturesForTeam(String teamName);

    List<Fixture> getFixturesForTeam(Team team);

}
