package com.football_analytics.statistics.services;

import com.football_analytics.statistics.entities.Team;
import com.football_analytics.statistics.models.DetailedStats;

public interface TeamService {

    Team getTeamByName(String teamName);

    DetailedStats getDetailedStatsForTeam(String teamName);
}
