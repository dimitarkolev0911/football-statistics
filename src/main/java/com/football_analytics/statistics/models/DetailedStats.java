package com.football_analytics.statistics.models;

import java.util.ArrayList;
import java.util.List;

import com.football_analytics.statistics.entities.Fixture;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class DetailedStats {

    private BasicStats basicStats;
    private List<Fixture> mostRecentFixtures;
    private List<Fixture> mostRecentFixturesAsHost;
    private List<Fixture> mostRecentFixturesAsAway;

    public static DetailedStats getDefaultDetailedStats() {
        return DetailedStats.builder()
                .basicStats(BasicStats.builder().build())
                .mostRecentFixtures(new ArrayList<>())
                .mostRecentFixturesAsHost(new ArrayList<>())
                .mostRecentFixturesAsAway(new ArrayList<>())
                .build();
    }

}
