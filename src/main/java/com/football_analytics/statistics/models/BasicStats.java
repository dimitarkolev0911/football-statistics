package com.football_analytics.statistics.models;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class BasicStats {

    private double avgGoals;
    private double avgGoalsFor;
    private double avgGoalsAgainst;

    private double avgCorners;
    private double avgCornersFor;
    private double avgCornersAgainst;

}
