package com.football_analytics.statistics.repositories;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import com.football_analytics.statistics.entities.Stadium;

public interface StadiumRepository extends JpaRepository<Stadium, UUID> {

}
