package com.football_analytics.statistics.repositories;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import com.football_analytics.statistics.entities.League;

public interface LeagueRepository extends JpaRepository<League, UUID> {

}
