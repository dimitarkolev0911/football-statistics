package com.football_analytics.statistics.repositories;

import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import com.football_analytics.statistics.entities.Team;

public interface TeamRepository extends JpaRepository<Team, UUID> {

    Optional<Team> findTeamByName(String name);

}
