package com.football_analytics.statistics.repositories;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.football_analytics.statistics.entities.Region;

@Repository
public interface RegionRepository extends JpaRepository<Region, UUID> {

}
