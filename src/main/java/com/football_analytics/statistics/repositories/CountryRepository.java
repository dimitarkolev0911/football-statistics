package com.football_analytics.statistics.repositories;

import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;

import com.football_analytics.statistics.entities.Country;

public interface CountryRepository extends JpaRepository<Country, UUID> {

}
