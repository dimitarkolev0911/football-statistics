package com.football_analytics.statistics.repositories;

import java.util.List;
import java.util.UUID;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.football_analytics.statistics.entities.Fixture;

public interface FixtureRepository extends JpaRepository <Fixture, UUID> {

    @Query(value = "SELECT * FROM fixtures WHERE home_team_id = :teamId OR away_team_id = :teamId", nativeQuery = true)
    List<Fixture> findFixturesForTeam(UUID teamId);

}
