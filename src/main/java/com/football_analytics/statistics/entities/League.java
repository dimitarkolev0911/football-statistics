package com.football_analytics.statistics.entities;

import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;

@Data
@Entity
@Table(name = "leagues")
public class League {

    @Id
    UUID id;

    @Column(name = "name")
    String name;

    @Column(name = "number_of_teams")
    Integer numberOfTeams;

    @ManyToOne
    @JoinColumn(name = "country_id")
    Country country;

    @JsonIgnore
    @ManyToMany(mappedBy = "leagues")
    Set<Team> teams;

    /*@JsonProperty(value = "teams")
    List<String> getTeamsNames() {
        return teams.stream().map(team -> team.name).collect(Collectors.toList());
    }*/
}
