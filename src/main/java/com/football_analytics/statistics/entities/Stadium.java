package com.football_analytics.statistics.entities;

import java.util.UUID;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
@Entity
@Table(name = "stadiums")
public class Stadium {

    @Id
    UUID id;

    @Column(name = "name")
    String name;

    @Column(name = "capacity")
    Integer capacity;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "country_id")
    Country country;
}
