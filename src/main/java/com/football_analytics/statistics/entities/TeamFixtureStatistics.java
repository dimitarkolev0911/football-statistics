package com.football_analytics.statistics.entities;

import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
@Entity
@Table(name = "team_statistics_for_fixture")
public class TeamFixtureStatistics extends Statistics {

    @Id
    private UUID id;

    @ManyToOne
    @JoinColumn(name = "team_id")
    @JsonIgnore
    private Team team;

    @JsonIgnore
    @ManyToOne
    private Fixture fixture;
}
