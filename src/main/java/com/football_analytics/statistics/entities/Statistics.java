package com.football_analytics.statistics.entities;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
@MappedSuperclass
public class Statistics {

    @Column(name = "goals")
    private Integer goals;

    @Column(name = "corners")
    private Integer corners;

    @JsonIgnore
    @Column(name = "throws")
    private Integer throwIns;

    @JsonIgnore
    @Column(name = "goal_kicks")
    private Integer goalKicks;

    @JsonIgnore
    @Column(name = "shots")
    private Integer shots;

    @JsonIgnore
    @Column(name = "shots_on_target")
    private Integer shotsOnTarget;

    @JsonIgnore
    @Column(name = "yellow_cards")
    private Integer yellowCards;

    @JsonIgnore
    @Column(name = "red_cards")
    private Integer redCards;

    @JsonIgnore
    @Column(name = "offsides")
    private Integer offsides;

    @JsonIgnore
    @Column(name = "fouls")
    private Integer fouls;
}
