package com.football_analytics.statistics.entities;

import java.util.Date;
import java.util.UUID;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
@Entity
@Table(name = "fixtures")
public class Fixture {

    @Id
    private UUID id;

    @ManyToOne
    @JoinColumn(name = "home_team_id")
    private Team homeTeam;

    @ManyToOne
    @JoinColumn(name = "away_team_id")
    private Team awayTeam;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "stadium_id")
    private Stadium stadium;

    @ManyToOne
    @JoinColumn(name = "league_id")
    private League league;

    @Column(name = "played_on")
    private Date date;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "home_team_stats_id")
    private TeamFixtureStatistics homeTeamFixtureStatistics;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "away_team_stats_id")
    private TeamFixtureStatistics awayTeamFixtureStatistics;
}
