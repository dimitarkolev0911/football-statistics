package com.football_analytics.statistics;

import lombok.Getter;

@Getter
public enum Errors {

    TEAM_NOT_FOUND("There is no team with such name."),
    INVALID_TEAM_NAME("The name of the team cannot be empty."),
    INVALID_TEAM("The team should have id.");

    private final String message;

    Errors(String message) {
        this.message = message;
    }
}
